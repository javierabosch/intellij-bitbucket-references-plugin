package com.atlassian.bitbucket.idea.rest;

import com.atlassian.bitbucket.idea.BitbucketUriBuilder;
import com.atlassian.bitbucket.idea.components.BitbucketUriBuilderProvider;
import com.atlassian.bitbucket.idea.configuration.BitbucketSettingsService;
import com.google.gson.JsonObject;
import com.intellij.openapi.project.Project;

import java.io.IOException;

public class DefaultRestClientService implements RestClient {

    private final Project project;
    private final BitbucketSettingsService settingsService;

    private RestClient delegate;

    private DefaultRestClientService(Project project, BitbucketSettingsService settingsService) {
        this.project = project;
        this.settingsService = settingsService;
    }

    @Override
    public JsonObject get(String path) throws IOException {
        return getRestClient().get(path);
    }

    @Override
    public int post(String path, String jsonBody) throws IOException {
        return getRestClient().post(path, jsonBody);
    }

    @Override
    public int delete(String path) throws IOException {
        return getRestClient().delete(path);
    }

    private RestClient getRestClient() {
        if (delegate == null) {
            BitbucketUriBuilderProvider uriBuilderProvider = project.getComponent(BitbucketUriBuilderProvider.class);
            BitbucketUriBuilder bitbucketUriBuilder = uriBuilderProvider.getBitbucketUriBuilder(project.getBaseDir());

            if (bitbucketUriBuilder != null) {
                delegate = new DefaultRestClient(bitbucketUriBuilder.repositoryRestUri(), settingsService);
            } else {
                throw new IllegalStateException("No remote Bitbucket");
            }
        }
        return delegate;
    }
}

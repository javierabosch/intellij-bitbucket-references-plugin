package com.atlassian.bitbucket.idea.pullrequest.change;

import java.util.List;

public interface ChangedFilesProvider {

    List<ChangedFile> getChanges();
}

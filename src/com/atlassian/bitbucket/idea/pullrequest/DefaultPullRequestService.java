package com.atlassian.bitbucket.idea.pullrequest;

import com.atlassian.bitbucket.idea.pullrequest.change.ChangedFile;
import com.atlassian.bitbucket.idea.pullrequest.change.ChangedFilesProvider;
import com.atlassian.bitbucket.idea.pullrequest.change.DefaultChangedFilesProvider;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentsProvider;
import com.atlassian.bitbucket.idea.pullrequest.comment.DefaultCommentsProvider;
import com.atlassian.bitbucket.idea.pullrequest.rest.PullRequestRestApi;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.text.StringUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class DefaultPullRequestService implements PullRequestService {

    private final Project project;
    private final PullRequestRestApi restApi;

    private PullRequest activePullRequest;

    private DefaultPullRequestService(Project project) {
        this.project = project;
        restApi = ServiceManager.getService(project, PullRequestRestApi.class);
    }

    @Nullable
    @Override
    public PullRequest getActivePullRequest() {
        return activePullRequest;
    }

    public void setActivePullRequest(@Nullable PullRequest pullRequest) {
        activePullRequest = pullRequest;
    }


    @Override
    public ChangedFilesProvider getChangedFileProvider() {
        if (activePullRequest != null) {
            return new DefaultChangedFilesProvider(restApi, activePullRequest.getId());
        }
        return null;
    }

    @Override
    public CommentsProvider getCommentsProvider(ChangedFile changedFile) {
        if (activePullRequest != null) {
            return new DefaultCommentsProvider(restApi, activePullRequest.getId(), changedFile.getRelativePath());
        }
        return null;
    }

    @NotNull
    @Override
    public List<PullRequest> getOpenPullRequests(@NotNull Repository repository) {
        DefaultPullRequestProvider pullRequestProvider = new DefaultPullRequestProvider(restApi);
        return pullRequestProvider.getOpenPullRequests();

        // TODO: Remove dummy code when we're happy with getting data from BbS
        //return Lists.<PullRequest>newArrayList(DefaultPullRequest.build(1,
        //        DefaultUser.builder()
        //                .displayName("Brent Plump")
        //                .build(),
        //        new Date(),
        //        "Awesome PR"));
    }

    @NotNull
    @Override
    public String resolveRelativePath(@NotNull VirtualFile virtualFile) {
        String relativePath = StringUtil.trimStart(virtualFile.getPath(), project.getBaseDir().getPath());
        return StringUtil.trimLeading(relativePath, '/');
    }
}
package com.atlassian.bitbucket.idea.pullrequest.comment;

import com.atlassian.bitbucket.idea.pullrequest.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.List;

public interface Comment {

    @Nullable
    CommentAnchor getAnchor();

    long getId();

    /**
     * Raw markdown text of commit
     */
    @NotNull
    String getText();

    @Nullable
    String getHtmlText();

    @NotNull
    User getCommentator();

    @NotNull
    Date getCreatedDate();

    @NotNull
    Date getUpdatedDate();

    @NotNull
    List<Comment> getReplyComments();

    boolean isEditable();

    boolean isDeletable();

    int getVersion();
}

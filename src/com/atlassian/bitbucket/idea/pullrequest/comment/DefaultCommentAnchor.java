package com.atlassian.bitbucket.idea.pullrequest.comment;

public class DefaultCommentAnchor implements CommentAnchor {
    private final int lineNumber;
    private final FileType fileType;
    private final LineType lineType;

    private DefaultCommentAnchor(int lineNumber, FileType fileType, LineType lineType) {
        this.lineNumber = lineNumber;
        this.fileType = fileType;
        this.lineType = lineType;
    }

    public static DefaultCommentAnchor anchor(int lineNumber, FileType fileType, LineType lineType) {
        return new DefaultCommentAnchor(lineNumber, fileType, lineType);
    }

    @Override
    public FileType getFileType() {
        return fileType;
    }

    @Override
    public int getLineNumber() {
        return lineNumber;
    }

    @Override
    public LineType getLineType() {
        return lineType;
    }
}

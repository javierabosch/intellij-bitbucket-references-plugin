package com.atlassian.bitbucket.idea.pullrequest.comment.model;

import com.atlassian.bitbucket.idea.pullrequest.comment.Comment;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentAnchor;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentsProvider;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents comments in one file
 */
public class FileCommentsModel {

    private final CommentsProvider commentsProvider;
    /**
     * Path to the file with comments
     */
    // TODO seems to be redundant
    private final String filePath;
    /**
     * Map of {@link LineCommentsBlock} to reuse existing objects
     */
    private final Map<Integer, LineCommentsBlock> lineCommentsBlocks;

    public FileCommentsModel(CommentsProvider commentsProvider, String filePath) {
        this.commentsProvider = commentsProvider;
        this.filePath = filePath;
        this.lineCommentsBlocks = new HashMap<Integer, LineCommentsBlock>();
    }

    public String getFilePath() {
        return filePath;
    }

    public Collection<LineCommentsBlock> getLineCommentsBlocks() {
        for (Comment comment : commentsProvider.getComments()) {
            CommentAnchor anchor = comment.getAnchor();

            // TODO support FROM comments too
            if (anchor != null && anchor.getFileType() == CommentAnchor.FileType.TO) {
                int lineNumber = anchor.getLineNumber();
                LineCommentsBlock commentsBlock = lineCommentsBlocks.get(lineNumber);

                if (commentsBlock == null) {
                    lineCommentsBlocks.put(lineNumber, new LineCommentsBlock(lineNumber, commentsProvider));
                }
            }
        }

        return Collections.unmodifiableCollection(lineCommentsBlocks.values());
    }

}
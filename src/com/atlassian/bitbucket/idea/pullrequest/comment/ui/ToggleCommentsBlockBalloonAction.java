package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.atlassian.bitbucket.linky.Constants;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.project.DumbAwareAction;
import org.jetbrains.annotations.NotNull;

public class ToggleCommentsBlockBalloonAction extends DumbAwareAction {
    private static final String DESCRIPTION = "Show/hide pull request reviewer's comments for this line";
    private static final String TEXT = "Toggle comments for this line";

    private final Runnable actionRunnable;

    private boolean visible;

    public ToggleCommentsBlockBalloonAction(@NotNull Runnable actionRunnable) {
        super(TEXT, DESCRIPTION, Constants.BITBUCKET_ICON);
        this.actionRunnable = actionRunnable;
        visible = false;
    }

    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        actionRunnable.run();
    }
}

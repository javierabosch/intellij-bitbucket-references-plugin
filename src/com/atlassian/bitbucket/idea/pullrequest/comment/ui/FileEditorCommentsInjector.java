package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.idea.pullrequest.PullRequestService;
import com.atlassian.bitbucket.idea.pullrequest.change.ChangedFile;
import com.atlassian.bitbucket.idea.pullrequest.change.ChangedFilesProvider;
import com.atlassian.bitbucket.idea.pullrequest.comment.CommentsProvider;
import com.atlassian.bitbucket.idea.pullrequest.comment.model.FileCommentsModel;
import com.google.common.io.Resources;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.fileEditor.*;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import javax.imageio.ImageIO;
import javax.swing.border.Border;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class FileEditorCommentsInjector implements FileEditorManagerListener {
    private static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);

    private final PullRequestService pullRequestService;
    private final Map<VirtualFile, FileCommentsRenderer> renderers;

    public FileEditorCommentsInjector(PullRequestService pullRequestService) {
        this.pullRequestService = pullRequestService;
        this.renderers = new HashMap<VirtualFile, FileCommentsRenderer>();
    }

    @Override
    public void fileOpened(@NotNull FileEditorManager fileEditorManager, @NotNull VirtualFile virtualFile) {
        FileEditor[] allEditors = fileEditorManager.getEditors(virtualFile);
        for (FileEditor editor : allEditors) {
            inject(editor, virtualFile);
        }
    }

    @Override
    public void fileClosed(@NotNull FileEditorManager fileEditorManager, @NotNull VirtualFile virtualFile) {
        FileCommentsRenderer renderer = renderers.remove(virtualFile);
        if (renderer != null) {
            renderer.dispose();
        }
    }

    @Override
    public void selectionChanged(@NotNull FileEditorManagerEvent fileEditorManagerEvent) {
        VirtualFile oldFile = fileEditorManagerEvent.getOldFile();
        FileCommentsRenderer renderer = renderers.get(oldFile);
        if (renderer != null) {
            renderer.hidePopups();
        }
    }

    private void inject(FileEditor fileEditor, VirtualFile virtualFile) {
        if (!(fileEditor instanceof TextEditor)) {
            log.debug("Injection failed, only text editors are supported currently.");
            return;
        }

        TextEditor textEditor = (TextEditor) fileEditor;

        ChangedFilesProvider changedFileProvider = pullRequestService.getChangedFileProvider();
        if (changedFileProvider == null) {
            return;
        }

        try {
            URL charlieUrl = Resources.getResource(getClass(), "/charlie.png");
            BufferedImage image = ImageIO.read(charlieUrl);
            Border background = new BackgroundImage(image);
            textEditor.getEditor().getContentComponent().setBorder(background);
        } catch (Exception e) {
            log.warn("Failed to set background", e);
        }

        String relativePath = pullRequestService.resolveRelativePath(virtualFile);

        Collection<ChangedFile> changedFiles = changedFileProvider.getChanges();
        for (ChangedFile changedFile : changedFiles) {
            if (relativePath.equals(changedFile.getRelativePath())) {

                CommentsProvider commentsProvider = pullRequestService.getCommentsProvider(changedFile);
                FileCommentsModel commentsModel = new FileCommentsModel(commentsProvider, relativePath);
                FileCommentsRenderer commentsRenderer = new FileCommentsRenderer(textEditor.getEditor(), commentsModel);
                renderers.put(virtualFile, commentsRenderer);
            }
        }
    }
}

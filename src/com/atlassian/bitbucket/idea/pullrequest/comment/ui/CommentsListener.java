package com.atlassian.bitbucket.idea.pullrequest.comment.ui;

import com.intellij.util.messages.Topic;

public interface CommentsListener {

    Topic<CommentsListener> COMMENTS_UPDATED_TOPIC = Topic.create("comments updated", CommentsListener.class);

    void commentsUpdated();
}

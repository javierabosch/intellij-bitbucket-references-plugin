package com.atlassian.bitbucket.idea.pullrequest.rest;

/*
 * Represents Comment Anchor:
 *
 * {
 *     "fromHash": "96f31e7a9061192660550dbbe982c682b52d4668",
 *     "toHash": "248e629ace89eafb17d6336ecc0e3fb32bccb820",
 *     "line": 12,
 *     "lineType": "CONTEXT",
 *     "fileType": "FROM",
 *     "path": "README.md"
 * }
 */
public class RestCommentAnchor {
    private int line;
    private String lineType;
    private String fileType;
    private String path;

    static RestCommentAnchor build(int line, String fileType, String lineType, String path) {
        RestCommentAnchor restCommentAnchor = new RestCommentAnchor();
        restCommentAnchor.line = line;
        restCommentAnchor.lineType = lineType;
        restCommentAnchor.fileType = fileType;
        restCommentAnchor.path = path;
        return restCommentAnchor;
    }

    public int getLine() {
        return line;
    }

    public String getFileType() {
        return fileType;
    }

    public String getLineType() {
        return lineType;
    }

    public String getPath() {
        return path;
    }
}


package com.atlassian.bitbucket.idea.pullrequest.rest;

/**
 * {
 * "text": "A measured reply.",
 * "parent": {
 * "id": 1
 * }
 * }
 * <p/>
 * or
 * <p/>
 * {
 * "text": "A pithy comment on a particular line within a file.",
 * "anchor": {
 * "line": 1,
 * "lineType": "CONTEXT",
 * "fileType": "FROM"
 * "path": "path/to/file",
 * "srcPath": "path/to/file"
 * }
 * }
 */
public class RestPostComment {
    private String text;
    private Parent parent;
    private RestCommentAnchor anchor;

    public static RestPostComment reply(long parentId, String text) {
        RestPostComment reply = new RestPostComment();
        reply.text = text;
        reply.parent = new Parent();
        reply.parent.id = parentId;
        return reply;
    }

    public static RestPostComment create(int lineNumber, String lineType, String filePath, String text) {
        RestPostComment comment = new RestPostComment();
        comment.text = text;
        comment.anchor = RestCommentAnchor.build(lineNumber, "TO", lineType, filePath);
        return comment;
    }

    private static class Parent {
        private long id;
    }

}

package com.atlassian.bitbucket.linky;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class Constants {

    /**
     * Second- and top-level domains of Bitbucket Cloud.
     * Do not use direct string comparison as there might be third-level domains (i.e. {@code staging.bitbucket.org}).
     */
    public static final String BITBUCKET_CLOUD_HOST = "bitbucket.org";
    public static final String BITBUCKET_CLOUD_DEV_HOST = "bb-inf.net";

    /**
     * Icon that should be used for actions related to Bitbucket
     */
    public static final Icon BITBUCKET_ICON = IconLoader.getIcon("/icons/bitbucket.png");

    /**
     * Key that should be used to create {@link org.slf4j.Logger} instance.
     * It is easier to filter {@code idea.log} by just one expression
     * while troubleshooting.
     */
    public static final String LOGGER_NAME = "BitbucketReferences";

    private Constants() {
    }
}

package com.atlassian.bitbucket.linky.hosting;

import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class DefaultHosting implements Hosting {
    private final Optional<URI> baseRestUri;
    private final URI baseUri;
    private final Type type;

    private DefaultHosting(Builder builder) {
        baseRestUri = requireNonNull(builder.baseRestUri, "builder.baseRestUri");
        baseUri = requireNonNull(builder.baseUri, "builder.baseUri");
        type = requireNonNull(builder.type, "builder.type");
    }

    public static Builder cloud() {
        return new Builder(Type.CLOUD);
    }

    public static Builder server() {
        return new Builder(Type.SERVER);
    }

    @NotNull
    @Override
    public Optional<URI> getBaseRestUri() {
        return baseRestUri;
    }

    @NotNull
    @Override
    public URI getBaseUri() {
        return baseUri;
    }

    @NotNull
    @Override
    public Type getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DefaultHosting that = (DefaultHosting) o;
        return Objects.equals(baseRestUri, that.baseRestUri) &&
                Objects.equals(baseUri, that.baseUri) &&
                type == that.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(baseRestUri, baseUri, type);
    }

    public static final class Builder {
        private final Type type;
        private URI baseUri;
        private Optional<URI> baseRestUri = empty();

        private Builder(Type type) {
            this.type = type;
        }

        @NotNull
        public Builder baseUri(@NotNull URI baseUri) {
            this.baseUri = requireNonNull(baseUri, "baseUri");
            return this;
        }

        @NotNull
        public Builder baseRestUri(@NotNull URI baseRestUri) {
            this.baseRestUri = of(requireNonNull(baseRestUri, "baseRestUri"));
            return this;
        }

        @NotNull
        public DefaultHosting build() {
            return new DefaultHosting(this);
        }
    }
}

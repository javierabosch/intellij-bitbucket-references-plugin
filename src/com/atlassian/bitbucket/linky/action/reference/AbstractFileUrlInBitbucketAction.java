package com.atlassian.bitbucket.linky.action.reference;

import com.atlassian.bitbucket.linky.BitbucketLinky;
import com.atlassian.bitbucket.linky.LinesSelection;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.SelectionModel;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.AbstractVcs;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.util.ArrayList;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;

public abstract class AbstractFileUrlInBitbucketAction extends AbstractBitbucketReferenceAction {

    @Override
    protected final void actionPerformed(@NotNull AnActionEvent event,
                                         @NotNull Project project,
                                         @NotNull VirtualFile file,
                                         @NotNull BitbucketLinky linky,
                                         @NotNull AbstractVcs vcs) {
        URI linkUri = getUriToFileSourceView(
                linky,
                file,
                CommonDataKeys.EDITOR.getData(event.getDataContext()));

        actionPerformed(event.getProject(), linkUri);
    }

    protected abstract void actionPerformed(@Nullable Project project, @NotNull URI fileUri);

    private URI getUriToFileSourceView(BitbucketLinky linky, VirtualFile file, @Nullable Editor editor) {
        if (editor == null) {
            return linky.getSourceViewUri(file);
        }

        SelectionModel selectionModel = editor.getSelectionModel();
        int[] starts = selectionModel.getBlockSelectionStarts();
        int[] ends = selectionModel.getBlockSelectionEnds();

        Document document = editor.getDocument();
        ArrayList<LinesSelection> linesSelections = new ArrayList<>(starts.length);
        for (int i = 0; i < starts.length; i++) {
            getLineSelection(document, starts[i], ends[i])
                    .ifPresent(linesSelections::add);
        }
        return linky.getSourceViewUri(file, linesSelections);
    }

    private Optional<LinesSelection> getLineSelection(Document document, int selectionStartOffset, int selectionEndOffset) {
        int startLineNumber = document.getLineNumber(selectionStartOffset);
        int startLineEndOffset = document.getLineEndOffset(startLineNumber);
        // exclude first line if selection starts from its very end
        if (selectionStartOffset == startLineEndOffset) {
            startLineNumber++;
        }

        int endLineNumber = document.getLineNumber(selectionEndOffset);
        int endLineStartOffset = document.getLineStartOffset(endLineNumber);
        // exclude last line if selection ends on its very beginning
        if (selectionEndOffset == endLineStartOffset) {
            endLineNumber--;
        }

        return startLineNumber <= endLineNumber ?
                of(LinesSelection.of(startLineNumber + 1, endLineNumber + 1)) :
                empty();
    }
}

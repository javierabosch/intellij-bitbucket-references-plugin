package com.atlassian.bitbucket.linky.discovery;

import com.atlassian.bitbucket.linky.discovery.git.GitRemoteAnalyzer;
import com.atlassian.bitbucket.linky.discovery.git.GitRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.discovery.hg.HgRemoteAnalyzer;
import com.atlassian.bitbucket.linky.discovery.hg.HgRepositoryDiscoverer;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.atlassian.bitbucket.linky.service.VcsAvailabilityService;
import com.atlassian.bitbucket.linky.service.blame.GitBlameLineService;
import com.atlassian.bitbucket.linky.service.blame.HgBlameLineService;
import com.atlassian.bitbucket.linky.service.hosting.HostingRegistry;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vcs.ProjectLevelVcsManager;
import com.intellij.openapi.vcs.VcsListener;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.util.messages.MessageBus;
import git4idea.GitUtil;
import org.jetbrains.annotations.NotNull;
import org.zmlx.hg4idea.util.HgUtil;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Consumer;

import static com.atlassian.bitbucket.linky.Constants.LOGGER_NAME;
import static java.lang.String.format;
import static java.util.Collections.emptyMap;
import static java.util.Collections.unmodifiableMap;
import static java.util.Optional.empty;
import static java.util.Optional.of;

public class BitbucketRepositoryDiscoveryComponent extends AbstractProjectComponent
        implements VcsRepositoryDiscoverer, VcsListener {

    private static final Duration DISCOVERY_DELAY = Duration.ofMinutes(10);

    private static final Logger log = Logger.getInstance(LOGGER_NAME);

    private final BitbucketDiscoveryTask discoveryTask;
    private final AtomicReference<Map<VirtualFile, BitbucketRepository>> repositories;

    private ScheduledExecutorService discoveryService;

    public BitbucketRepositoryDiscoveryComponent(Project project) {
        super(project);
        this.repositories = new AtomicReference<>(emptyMap());
        this.discoveryTask = new BitbucketDiscoveryTask(project, this::updateRepositories);
    }

    @Override
    public synchronized void directoryMappingChanged() {
        if (discoveryService != null) {
            discoveryService.shutdownNow();
        }

        log.debug("Scheduling Bitbucket discovery");
        discoveryService = createScheduledExecutor();

        discoveryService.scheduleAtFixedRate(discoveryTask, 0, DISCOVERY_DELAY.toMinutes(), TimeUnit.MINUTES);
    }

    @NotNull
    @Override
    public Map<VirtualFile, BitbucketRepository> discoverBitbucketRepositories() {
        return unmodifiableMap(repositories.get());
    }

    @Override
    public void initComponent() {
        MessageBus messageBus = myProject.getMessageBus();
        messageBus.connect().subscribe(ProjectLevelVcsManager.VCS_CONFIGURATION_CHANGED, this);
    }

    private ScheduledExecutorService createScheduledExecutor() {
        return Executors.newSingleThreadScheduledExecutor(
                new ThreadFactoryBuilder()
                        .setNameFormat("bitbucket-reference-plugin-remote-discoverer-%d")
                        .setDaemon(true)
                        .build());
    }

    private void updateRepositories(Map<VirtualFile, BitbucketRepository> discoveredRepositories) {
        repositories.set(discoveredRepositories);
    }


    private static class BitbucketDiscoveryTask implements Runnable {
        private final Project project;
        private final Consumer<Map<VirtualFile, BitbucketRepository>> discoveryConsumer;
        private final HostingRegistry hostingRegistry;
        private final Optional<VcsRepositoryDiscoverer> hgDiscoverer;
        private final Optional<VcsRepositoryDiscoverer> gitDiscoverer;
        private final VcsAvailabilityService vcsAvailabilityService;

        private BitbucketDiscoveryTask(Project project, Consumer<Map<VirtualFile, BitbucketRepository>> discoveryConsumer) {
            this.project = project;
            this.discoveryConsumer = discoveryConsumer;

            this.hostingRegistry = ServiceManager.getService(HostingRegistry.class);
            this.vcsAvailabilityService = ServiceManager.getService(VcsAvailabilityService.class);

            this.hgDiscoverer = createHgDiscoverer();
            this.gitDiscoverer = createGitDiscoverer();
        }

        @Override
        public void run() {
            log.debug(format("Started discovery of Bitbucket remotes for project '%s'", project.getName()));

            Map<VirtualFile, BitbucketRepository> discoveredRepositories = new HashMap<>();
            hgDiscoverer.ifPresent(d -> discoveredRepositories.putAll(d.discoverBitbucketRepositories()));
            gitDiscoverer.ifPresent(d -> discoveredRepositories.putAll(d.discoverBitbucketRepositories()));

            log.info(format("Discovered Bitbucket repositories: '%s'", discoveredRepositories));

            discoveryConsumer.accept(discoveredRepositories);
        }

        private Optional<VcsRepositoryDiscoverer> createGitDiscoverer() {
            return vcsAvailabilityService.isGitAvailable() ?
                    of(new GitRepositoryDiscoverer(
                            GitUtil.getRepositoryManager(project),
                            new GitRemoteAnalyzer(
                                    hostingRegistry,
                                    new BitbucketServerDetector(),
                                    ServiceManager.getService(project, GitBlameLineService.class)))) :
                    empty();
        }

        private Optional<VcsRepositoryDiscoverer> createHgDiscoverer() {
            return vcsAvailabilityService.isHgAvailable() ?
                    of(new HgRepositoryDiscoverer(
                            HgUtil.getRepositoryManager(project),
                            new HgRemoteAnalyzer(
                                    hostingRegistry,
                                    ServiceManager.getService(project, HgBlameLineService.class)))) :
                    empty();
        }
    }
}

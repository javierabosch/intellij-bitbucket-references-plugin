package com.atlassian.bitbucket.linky.discovery.hg;

import com.atlassian.bitbucket.linky.discovery.AbstractRemoteAnalyzer;
import com.atlassian.bitbucket.linky.repository.BitbucketRepository;
import com.atlassian.bitbucket.linky.repository.CloudBitbucketRepository;
import com.atlassian.bitbucket.linky.service.blame.BlameLineService;
import com.atlassian.bitbucket.linky.service.hosting.HostingRegistry;
import com.intellij.dvcs.repo.Repository;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;

import static com.atlassian.bitbucket.linky.repository.BitbucketRepository.VcsType.MERCURIAL;
import static java.lang.String.format;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;

public class HgRemoteAnalyzer extends AbstractRemoteAnalyzer {

    public HgRemoteAnalyzer(HostingRegistry hostingRegistry, BlameLineService blameLineService) {
        super(hostingRegistry, blameLineService);
    }

    @NotNull
    @Override
    public Optional<BitbucketRepository> bitbucketRepositoryFor(@NotNull Repository repository, @NotNull String remote) {
        log.debug(format("Analyzing hg remote url '%s'", remote));

        return parseRemote(remote.endsWith("/") ? remote : remote + "/")
                .flatMap(uri -> bitbucketRepositoryFor(repository, uri));
    }

    @NotNull
    private Optional<BitbucketRepository> bitbucketRepositoryFor(Repository repository, URI remoteUri) {
        String host = remoteUri.getHost();

        if (!ofNullable(host)
                .filter(this::containsBitbucketCloudHost)
                .isPresent()) {
            return empty();
        }

        return hostingRegistry.registerBitbucketCloud(host)
                .map(bb -> CloudBitbucketRepository.builder(repository.getRoot())
                        .bitbucket(bb)
                        .vcsType(MERCURIAL)
                        .vcsRepository(repository)
                        .blameLineService(blameLineService)
//                        .name() TODO yet no name for the repository
//                        .remoteName() TODO yet yno remote name for the repository
                        .uri(bb.getBaseUri().resolve(remoteUri.getPath()))
                        .vcsReferenceSupplier(() -> getCurrentVcsReference(repository))
                        .build());
    }
}

package com.atlassian.bitbucket.linky.repository;

import com.atlassian.bitbucket.linky.Constants;
import com.atlassian.bitbucket.linky.hosting.Hosting;
import com.atlassian.bitbucket.linky.service.blame.BlameLineService;
import com.intellij.dvcs.repo.Repository;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.vfs.VfsUtil;
import com.intellij.openapi.vfs.VirtualFile;
import org.jetbrains.annotations.NotNull;

import java.net.URI;
import java.util.Optional;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;

abstract class AbstractBitbucketRepository<B extends AbstractBitbucketRepository.Builder<B>> implements BitbucketRepository {

    protected static final Logger log = Logger.getInstance(Constants.LOGGER_NAME);

    protected final BlameLineService blameLineService;
    protected final Hosting hosting;
    protected final VcsType vcsType;
    protected final Repository vcsRepository;
    protected final Optional<String> name;
    protected final Optional<String> remoteName;
    protected final URI uri;
    protected final VirtualFile rootDirectory;
    protected final Supplier<String> vcsReferenceSupplier;

    protected AbstractBitbucketRepository(Builder<B> builder) {
        // TODO sanitize URIs
        this.hosting = requireNonNull(builder.hosting, "builder.hosting");
        this.vcsType = requireNonNull(builder.vcsType, "builder.vcsType");
        this.name = requireNonNull(builder.name, "builder.name");
        this.blameLineService = requireNonNull(builder.blameLineService, "builder.blameLineService");
        this.remoteName = requireNonNull(builder.remoteName, "builder.remoteName");
        this.uri = requireNonNull(builder.uri, "builder.uri");
        this.rootDirectory = requireNonNull(builder.rootDirectory, "builder.rootDirectory");
        this.vcsReferenceSupplier = requireNonNull(builder.vcsReferenceSupplier, "builder.vcsReferenceSupplier");
        this.vcsRepository = requireNonNull(builder.vcsRepository, "builder.vcsRepostiroy");
    }

    @NotNull
    public Hosting getHosting() {
        return hosting;
    }

    @NotNull
    @Override
    public VcsType getVcsType() {
        return vcsType;
    }

    @NotNull
    @Override
    public VirtualFile getRootDirectory() {
        return rootDirectory;
    }

    @NotNull
    @Override
    public Optional<String> getName() {
        return name;
    }

    @NotNull
    @Override
    public Optional<String> getRemoteName() {
        return remoteName;
    }

    @NotNull
    @Override
    public URI getUri() {
        return uri;
    }

    @NotNull
    protected String getCurrentVcsReference() {
        return vcsReferenceSupplier.get();
    }

    protected String getRelativeFilePath(VirtualFile file) {
        return VfsUtil.getRelativePath(file, rootDirectory);
    }

    public static abstract class Builder<B> {
        private final VirtualFile rootDirectory;

        private BlameLineService blameLineService;
        private Hosting hosting;
        private Optional<String> name;
        private Optional<String> remoteName;
        private URI uri;
        private VcsType vcsType;
        private Repository vcsRepository;
        private Supplier<String> vcsReferenceSupplier;

        protected Builder(@NotNull VirtualFile rootDirectory) {
            this.rootDirectory = requireNonNull(rootDirectory, "rootDirectory");
            this.name = empty();
            this.remoteName = empty();
        }

        @NotNull
        public B blameLineService(@NotNull BlameLineService blameLineService) {
            this.blameLineService = requireNonNull(blameLineService, "blameLineService");
            return self();
        }

        @NotNull
        public B bitbucket(@NotNull Hosting hosting) {
            this.hosting = requireNonNull(hosting, "hosting");
            return self();
        }

        @NotNull
        public B name(@NotNull Optional<String> name) {
            this.name = requireNonNull(name, "name");
            return self();
        }

        @NotNull
        public B remoteName(@NotNull Optional<String> remoteName) {
            this.remoteName = requireNonNull(remoteName, "remoteName");
            return self();
        }

        @NotNull
        public B uri(@NotNull URI uri) {
            this.uri = requireNonNull(uri, "uri");
            return self();
        }

        @NotNull
        public B vcsType(@NotNull BitbucketRepository.VcsType vcsType) {
            this.vcsType = requireNonNull(vcsType, "vcsType");
            return self();
        }

        @NotNull
        public B vcsRepository(@NotNull Repository vcsRepository) {
            this.vcsRepository = requireNonNull(vcsRepository, "vcsRepository");
            return self();
        }

        @NotNull
        public B vcsReferenceSupplier(@NotNull Supplier<String> vcsReferenceSupplier) {
            this.vcsReferenceSupplier = requireNonNull(vcsReferenceSupplier, "vcsReferenceSupplier");
            return self();
        }

        protected abstract B self();
    }
}
